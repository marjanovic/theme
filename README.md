# Gitea: Theme

[![Build Status](https://drone.gitea.com/api/badges/gitea/theme/status.svg)](https://drone.gitea.com/gitea/theme)
[![Join the chat at https://img.shields.io/discord/322538954119184384.svg](https://img.shields.io/discord/322538954119184384.svg)](https://discord.gg/NsatcWJ)

This is a Hugo theme that gets used within all of our websites like our blog, documentation and also the redirects.
If you commit any changes to the `master` branch it will trigger rebuilds of all the related websites.

## Install

You need an existing [Hugo](https://github.com/spf13/hugo) website, than you can just download our prebuilt [theme](https://dl.gitea.io/theme/master.tar.gz), put it into your `themes/gitea` folder and enable the theme with the `theme = "gitea"` option of your website.

## Development

We choose [npm](https://npmjs.org) to fetch our dependencies and [gulp](https://gulpjs.com/) for the pipeline.
We won't cover the installation of nodejs or npm, for that you can find enough guides depending on your operating system.
First of all you have to install the required dependencies:

```sh
npm install
```

If you want to do theme development we suggest to use the `watch` task we have defined to get the changes directly built after saving changes to a file:

```sh
npm run watch
```

Whenever you make a change to `src/main.scss` and save the file, this will trigger a rebuild of `static/styles/main.css`.
To see a preview of your changes on the website, do the following:

1. Go to gitea.io
2. Open the developer tools
3. Click the "Sources" tab
4. Open the `gitea.io/styles/main.css` file
5. Paste the entire `main.css` file from your local `gitea/theme` repo where `npm run watch` is running on

When you are done with your changes just create a pull request, after merging
the pull request the theme will be published to our [download page](https://dl.gitea.io/theme) automatically.

## Contributing

Fork -> Patch -> Push -> Pull Request

## Authors

* [Maintainers](https://github.com/orgs/go-gitea/people)
* [Contributors](https://github.com/go-gitea/theme/graphs/contributors)

## License

This project is under the Apache-2.0 License. See the [LICENSE](LICENSE) file for the full license text.

## Copyright

```none
Copyright (c) 2019 The Gitea Authors <https://gitea.io>
```
